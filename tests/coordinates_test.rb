require 'minitest/autorun'
require 'terminal-table'

require_relative '../app/coordinates.rb'
require_relative '../utils/io_test_helpers.rb'

class CoordinatesTest < Minitest::Test
  include IoTestHelpers

  describe Coordinates do
    def setup
      @coordinates = Coordinates.new
    end

    def test_that_measure_distance
      assert_equal 57.7944, @coordinates.distance([46.3625, 15.114444], [46.055556, 14.508333])
    end
  end
end
