require 'minitest/autorun'
require 'terminal-table'

require_relative '../app/miojo.rb'
require_relative '../utils/io_test_helpers.rb'

class MiojoTest < Minitest::Test
  include IoTestHelpers

  describe Miojo do
    def test_that_has_solution
      miojo = Miojo.new({ time_to_prepare: 3, hourglasses: [4, 5] })
      
      assert_equal 8, miojo.find_solution
    end

    def test_that_order_doesnt_matter
      miojo = Miojo.new({ time_to_prepare: 3, hourglasses: [5, 11] })
      miojo_2 = Miojo.new({ time_to_prepare: 3, hourglasses: [11, 5] })
      assert_equal miojo.find_solution, miojo_2.find_solution
    end

    def test_that_has_not_solution
      miojo = Miojo.new({ time_to_prepare: 3, hourglasses: [10, 30] })
      
      assert_equal 'N/A', miojo.find_solution
    end

    describe 'will read from terminal' do
      def setup
        @miojo = Miojo.new({ should_loop: true })
        @raises_exception = Validators.new('Deve ser um número')
        @input = -> (data) { IoTestHelpers.simulate_stdin(data) { @miojo.loadFromInput } }
      end
      
      it 'should read numbers' do
        @input.call(['3', '5', '7'])
  
        assert_equal 10, @miojo.find_solution
      end

      it 'should trigger not a number message' do
        assert_output(/Deve ser um número/) { 
          @input.call('a')
        }
      end

      it 'should trigger greater than 0 message' do
        assert_output(/O tempo deve ser maior que 0/) { 
          @input.call('-1')
        }
      end

      it 'should trigger not 0 message' do
        assert_output(/O tempo não pode ser 0/) { 
          @input.call('0')
        }
      end

      it 'should trigger hourglasses greater than time to prepare message' do
        assert_output(/O tempo das ampulhetas deve ser maior que o tempo de preparo/) { 
          @input.call(['3', '2'])
        }
      end

      it 'should trigger hourglasses must be different message' do
        assert_output(/O tempo das ampulhetas não pode ser igual/) { 
          @input.call(['3', '4', '4'])
        }
      end
    end

  end

end
