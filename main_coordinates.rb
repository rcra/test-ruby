require_relative './app/coordinates.rb'

class MainCoordinates
  def initialize
    @coordinates = Coordinates.new
  end

  def call
    begin
      @coordinates.loadFromInput
      @coordinates.display
      
      puts ''
      print 'Digite qualquer tecla para continuar e exit/quit para sair: '
    end until (a = gets.chomp) =~ /(?:ex|qu)it/i
  end
end

MainCoordinates.new.()