require 'terminal-table'
require_relative '../utils/exceptions.rb'

class Coordinates
  def initialize (params = {})
    @coordinates = params.fetch(:coordinates, [])
    @should_loop = params.fetch(:should_loop, true)
    @results = Array.new([])
  end

  def number?(obj)
    obj = obj.to_s unless obj.is_a? String
    /\A[+-]?\d+(\.[\d]+)?\z/.match(obj)
    /\A[+-]?\d+(\.[\d]+)?\z/.match(obj)
  end

  def invalid_data? (data)
    valid = true

    begin
      raise Validators.new('Não contém vírgula') if !data.include?(',')
      raise Validators.new('Deve conter somente dois pontos separados por vírgula') if data.count(',') > 2
      arr = data.split(",")
      raise Validators.new('Deve conter dois pontos') if arr.size != 2
      raise Validators.new('Deve ser um número decimal') if arr.all? { |n| !number?(n) }
      arr.map!{ |n| n.to_f }
      p arr[0] > 90.0
      raise Validators.new('Latitude deve estar entre 90 e -90 graus') if !arr[0].between?(-90.0, 90.0)
      raise Validators.new('Longitude deve estar entre 180 e -180 graus') if !arr[1].between?(-180.0, 180.0)
    rescue Validators => error
      valid = false
      puts "\n"
      puts "\e[31m#{error}\e[0m"
      puts "\n"
    end
    
    valid
  end

  #taken from https://www.it-swarm.dev/es/ruby/como-calcular-la-distancia-entre-dos-coordenadas-gps-sin-usar-la-api-de-google-maps/1069123582/
  def distance loc1, loc2
    rad_per_deg = Math::PI/180  # PI / 180
    rkm = 6371                  # Earth radius in kilometers
    rm = rkm * 1000             # Radius in meters

    dlat_rad = (loc2[0]-loc1[0]) * rad_per_deg  # Delta, converted to rad
    dlon_rad = (loc2[1]-loc1[1]) * rad_per_deg

    lat1_rad, lon1_rad = loc1.map {|i| i * rad_per_deg }
    lat2_rad, lon2_rad = loc2.map {|i| i * rad_per_deg }

    a = Math.sin(dlat_rad/2)**2 + Math.cos(lat1_rad) * Math.cos(lat2_rad) * Math.sin(dlon_rad/2)**2
    c = 2 * Math::atan2(Math::sqrt(a), Math::sqrt(1-a))

    ((rm * c) / 1000).round(4) # Delta in Km
  end

  def loadFromInput
    index = 0
    begin
      puts "As coordenadas deve ter o formato lat, long. Ex: 13.23423, 13.42524"
      puts "Digite 'c' para continuar"
      
      begin
        print "Digite a latitude e longitude do ponto #{index + 1}: "
        data = gets
        data ||= ''
        data.chomp!
        break if data == 'c' && index > 1
      end until invalid_data?(data)
      break if data == 'c' && index > 1

      coordinates = data.split(",").map { |s| s.to_f }
      @coordinates.push(coordinates)
      index += 1
    end while true || @should_loop

    calculate
  end

  def calculate
    i = 0
    limit = @coordinates.size

    while i < limit
      j = 0
      @results[i] = Array.new
      while j < limit
        @results[i].push(distance @coordinates[i], @coordinates[j])
        j += 1
      end
      i += 1
    end
  end

  def display
    i = 0
    limit = @results.size
    headings = ['-']
    
    table = Terminal::Table.new
    while i < limit
      headings << "Ponto #{i+1} #{@coordinates[i].to_s.gsub('[', '(').gsub(']', ')')}"
      @results[i].unshift("Ponto #{i+1}")
      i += 1
    end

    table.title = 'Distância entre os pontos'
    table.headings = headings
    table.rows = @results
    
    puts table
  end
end

# coordinates = nil
# coordinates = Coordinates.new()
# coordinates.loadFromInput
