require 'csv'
require 'terminal-table'
require_relative '../utils/exceptions.rb'

class Miojo
  def initialize (params = {})
    @time_to_prepare = params.fetch(:time_to_prepare, 1)
    @hourglasses = params.fetch(:hourglasses, [1, 2])
    @result = 0
    @should_loop = params.fetch(:should_loop, false)
  end

  def invalid_data? (data)
    valid = true
    begin
      raise Validators.new('Deve ser um número') if !data.is_a? Integer
      raise Validators.new('O tempo não pode ser 0') if data.zero?
      raise Validators.new('O tempo deve ser maior que 0') if data < 0
      raise Validators.new('O tempo das ampulhetas deve ser maior que o tempo de preparo') if data <= @time_to_prepare
      raise Validators.new('O tempo das ampulhetas não pode ser igual') if data == @hourglasses[0]
      
    rescue Validators => error
      valid = false
      puts "\n"
      puts "\e[31m#{error}\e[0m"
      puts "\n"
    end
    
    valid
  end

  def find_solution
    counter = 1
    hourglasses = @hourglasses.sort
    while counter <= hourglasses[0] do
      if ((hourglasses[0] * counter) % hourglasses[1] == @time_to_prepare)
        return counter * hourglasses[0]
      end
      counter += 1
    end

    'N/A'
  end

  def readInput(prompt)
    begin 
      print prompt
      data = Integer (gets.chomp) rescue false
    end until invalid_data?(data) || @should_loop
    data
  end

  def display
    table = Terminal::Table.new :headings => ['Tempo de preparo', 'Ampulheta 1', 'Ampulheta 2', 'Tempo necessário'] do |row|
      row << [@time_to_prepare, @hourglasses[0], @hourglasses[1], @result || 'N/A']
    end
    puts table
  end

  def loadFromInput
    @time_to_prepare = readInput('Digite o tempo de preparo do miojo: ')
    @hourglasses[0] = readInput('Digite a duração da primeira ampulheta: ')
    @hourglasses[1] = readInput('Digite a duração da segunda ampulheta: ')
  end

  def loadFromFile
    # TODO: Validações nos arquivos
    file = CSV.parse(File.read('./files/miojo.csv'), headers: false, converters: :integer)
    i = 1
    while i < file.size
      @time_to_prepare = file[i][0]
      @hourglasses[0] = file[i][1]
      @hourglasses[1] = file[i][2]

      find_solution

      i += 1
    end
  end

end

# miojo = nil
# miojo = Miojo.new
# miojo.loadFromInput

# isFile = true
# if isFile
#   miojo.loadFromFile
# else 
#   miojo.loadFromInput
#   miojo.display
# end

