class Validators < StandardError
  attr_reader :type
  def initialize(message = "Erro genérico", type = 'Error')
    @type = type
    super(message)
  end
end
