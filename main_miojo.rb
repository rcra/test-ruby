require_relative './app/miojo.rb'

class MainMiojo
  def initialize
    @miojo = Miojo.new
  end

  def call
    begin
      @miojo.loadFromInput
      @miojo.find_solution
      @miojo.display
      
      puts ''
      puts 'Digite qualquer tecla para continuar e exit/quit para sair'
    end until (a = gets.chomp) =~ /(?:ex|qu)it/i
  end
end

MainMiojo.new.()